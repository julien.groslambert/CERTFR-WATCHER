# CERTFR-WATCHER

Permet de lister tous les avis émis par [cert.ssi.gouv.fr](https://www.cert.ssi.gouv.fr)

Licence: [Apache License 2.0](./LICENCE)

## Compilation

Utiliser [maven 3](http://maven.apache.org/) :

Construire le package :

```bash
mvn package
```

Executer les tests unitaires :

```bash
mvn test
```
