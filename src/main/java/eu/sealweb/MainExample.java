package eu.sealweb;

public class MainExample {

    public static void main(String[] args) throws Exception {
        CERTFR certfr = new CERTFR(164, 209);

        certfr.addBlackListedApp("Chrome");
        certfr.addBlackListedApp("Android");

        certfr.create_report();
    }

}
