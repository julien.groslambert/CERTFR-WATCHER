package eu.sealweb;

import core.printing.Doc;
import core.printing.table.SimpleTable;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.net.ssl.SSLContext;
import java.io.*;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class CERTFR {

    private final int start;
    private final int end;
    private final List<String> blacklist = new ArrayList<>();
    private final List<String> inside_perimer = new ArrayList<>();
    private final List<String> outside_perimeter = new ArrayList<>();

    public CERTFR(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public void create_report() throws Exception {
        Doc doc = new Doc();

        doc.addSection("Tâches par projet");
        SimpleTable tasktable = doc.addSimpleTable();
        tasktable.add("Objet");
        if (this.hasPerimeter()) {
            tasktable.add("Périmètre");
        }
        tasktable.add("Avis ou Alerte");
        tasktable.add("Risque");
        tasktable.add("Système");
        tasktable.add("Résumé");
        tasktable.add("Date");
        tasktable.add("Source");
        tasktable.add("Correctif");
        for (int ident = this.start; ident <= this.end; ident++) {
            System.out.println("Traitement avis: " + this.getIdentOnThreeDigit(ident));
            StringBuffer resp = this.downloadfiles(ident);
            String title = this.extractVulnerabilityTitle(resp);
            if (!isBlackListed(title)) {

                tasktable.newline();
                tasktable.add(title);
                if (hasPerimeter()) {
                    if (isInLot8(title)) {
                        tasktable.add("oui");
                    } else if (isOutLot8(title)) {
                        tasktable.add("non");
                    } else {
                        tasktable.add("");
                    }
                }
                tasktable.add("Avis");
                tasktable.add(this.extractRisk(resp));
                tasktable.add(this.extractSysteme(resp));

                tasktable.add(this.extractSummary(resp));
                tasktable.add(this.extractDate(resp));
                tasktable.add("https://www.cert.ssi.gouv.fr/avis/CERTFR-2018-AVI-" + this.getIdentOnThreeDigit(ident) + "/");
                tasktable.add(this.extractDocumentation(resp));
            }

        }
        System.out.println("Traitement terminé");

        doc.pop();

        Writer fw = new FileWriter(new File("out/vulnerabilite.html"));
        fw.write(doc.getHTML());
        fw.close();
    }

    private boolean hasPerimeter() {
        return !(this.inside_perimer.isEmpty() && this.outside_perimeter.isEmpty());
    }

    private String getIdentOnThreeDigit(int ident) {
        if (ident == 2) {
            return "" + ident;
        }
        if (ident < 10) {
            return "00" + ident;
        }
        if (ident < 100) {
            return "0" + ident;
        }
        return ident + "";
    }

    private boolean isBlackListed(String ident) {
        for (String s : this.blacklist) {
            if (ident.contains(s)) {
                return true;
            }
        }
        return false;
    }

    private boolean isInLot8(String ident) {
        for (String s : this.inside_perimer) {
            if (ident.contains(s)) {
                return true;
            }
        }
        return false;
    }

    private boolean isOutLot8(String ident) {
        for (String s : this.outside_perimeter) {
            if (ident.contains(s)) {
                return true;
            }
        }
        return false;
    }

    private String extractVulnerabilityTitle(StringBuffer resp) {
        String title_balise = "<td class=\"col-xs-4\">Titre</td>";
        int begin = resp.indexOf(title_balise);
        int end = resp.indexOf("</tr>", begin);
        return resp.substring(begin + title_balise.length() + 22, end - 6);
    }

    private String extractDate(StringBuffer resp) {
        String title_balise = "<td class=\"col-xs-4\">Date de la première version</td>";
        int begin = resp.indexOf(title_balise);
        int end = resp.indexOf("</tr>", begin);
        return resp.substring(begin + title_balise.length() + 22, end - 6);
    }

    private String extractRisk(StringBuffer resp) {
        String title_balise = "<h2>Risque(s)</h2>";
        int begin = resp.indexOf(title_balise);
        int end = resp.indexOf("</ul>", begin);
        return resp.substring(begin + 1 + title_balise.length(), end);
    }

    private String extractDocumentation(StringBuffer resp) {
        String title_balise = "<h2>Documentation</h2>";
        int begin = resp.indexOf(title_balise);
        int end = resp.indexOf("</ul>", begin);
        return resp.substring(begin + 1 + title_balise.length(), end + 5);
    }

    private String extractSummary(StringBuffer resp) {
        String title_balise = "<h2>Résumé</h2>";

        int begin = resp.indexOf(title_balise);
        int end = resp.indexOf("<h2>", begin + 2);
        return resp.substring(begin + 4 + title_balise.length(), end);
    }

    private String extractSysteme(StringBuffer resp) {
        String title_balise = "<h2>Systèmes affectés</h2>";
        int begin = resp.indexOf(title_balise);
        if (begin != -1) {
            int end = resp.indexOf("<h2>", begin + 1);
            String result = resp.substring(begin + 1 + title_balise.length(), end);
            return result;
        }
        return "non renseigné";
    }

    private StringBuffer downloadfiles(int ident) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, IOException, ClientProtocolException {
        SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(null, new TrustStrategy() {

            @Override
            public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                // Trust all certificates..
                return true;
            }
        }).build();

        // Allow TLSv1 protocol only
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, null, null,
                SSLConnectionSocketFactory.getDefaultHostnameVerifier());
        CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
        HttpGet httpget = new HttpGet("https://www.cert.ssi.gouv.fr/avis/CERTFR-2018-AVI-"
                + this.getIdentOnThreeDigit(ident) + "/");
        HttpResponse response = httpclient.execute(httpget);

        InputStream is = response.getEntity().getContent();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        StringBuffer resp = new StringBuffer();
        String line;
        while ((line = rd.readLine()) != null) {
            resp.append(line);
            resp.append('\r');
        }
        rd.close();

        Writer fw = new FileWriter("out/" + "CERTFR-2018-AVI-" + this.getIdentOnThreeDigit(ident) + ".html");
        fw.write(resp.toString());
        fw.close();

        return resp;
    }

    public void addBlackListedApp(String app) {
        this.blacklist.add(app);
    }

    public void addInsidePerimeter(String string) {
        this.inside_perimer.add(string);
    }

    public void addOutsidePerimeter(String string) {
        this.outside_perimeter.add(string);
    }

}
